# config valid only for current version of Capistrano
lock '3.3.5'

app_name = "esdm"

set :application, 'esdm'
set :repo_url, 'git@bitbucket.org:kubid/esdm.git'
set :deploy_to, "/home/#{ app_name }"
set :scm, :git
set :default_stage, "production"
set :unicorn_conf, "#{current_path}/config/unicorn.rb"
set :unicorn_pid, "#{shared_path}/pids/unicorn.pid"


namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
