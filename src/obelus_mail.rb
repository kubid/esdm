class ObelusMail

  def self.send_pathway(params)
    send_to 'calculator.2050.indonesia@gmail.com', default_text(params)
    send_to 'kubido@gmail.com', default_text(params)
    send_to params["list_email"], friends_text(params)
  end

  def self.default_text(params)
    namespace = OpenStruct.new(params)
    {
      subject: "[PATHWAY SUBMIT] - #{params["nama"]}",
      body: ERB.new(File.read('src/email_admin.erb')).result(namespace.instance_eval {binding})
    }
  end

  def self.friends_text(params)
    namespace = OpenStruct.new(params)
    {
      subject: "#{params["nama"]} ingin berbagi Skenario 2050",
      body: ERB.new(File.read('src/email.erb')).result(namespace.instance_eval {binding})
    }

  end

  def self.send_to(emails, text)
    Pony.mail from: 'noreply@calculator2050.esdm.go.id',
              to: emails,
              subject: text[:subject],
              body: text[:body],
              via: :smtp,
              headers: { 'Content-Type' => 'text/html' },
              via_options: {
                address:    'smtp.sendgrid.net',
                port:       '25',
                user_name:  'app36806698@heroku.com',
                password:   'znutke7f2222',        
                domain:     'esdm.obelus-technology.com', # the HELO domain provided by the client to the server
                authentication: :plain # :plain, :login, :cram_md5, no auth by default
              }
  end

end