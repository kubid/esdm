# encoding: utf-8
require 'sinatra'
require 'erb'
require 'json'
require 'net/http'

require_relative 'obelus_mail'
require_relative '../model/data_from_model'

# This provides useful scripts for the index.html.erb file
module Helper

  def assets
    # In production, these are overwritten with precompiled versions
    @assets ||= { 'application.css' => 'application.css', 'application.js' => 'application.js' }
  end

  def assets=(h)
    @assets = h
  end

  def structure
    @structure ||= DataFromModel.new
  end

  def paper
    files = {}
    path  =  File.dirname(__FILE__).gsub('/src', '')+ '/public/assets/mini_paper'
    paper_dir = Dir.entries(path).reject{|x| x[0] == '.'}
    paper_dir.each do |pdir|
      pdir_path = "#{path}/#{pdir}"
      files[pdir] = {id: [], en: []}
      files[pdir][:id] = Dir.entries("#{pdir_path}/id").reject{|x| x[0] == '.'}
      files[pdir][:en] = Dir.entries("#{pdir_path}/en").reject{|x| x[0] == '.'}
    end
    @mini_paper = files
  end


end

# This deals with urls that relate to previous versions of the 2050 calculator.
# If you are developing your own calculator, delete from here to the line marked STOP DELETING HERE
class TwentyFiftyServer < Sinatra::Base

  enable :lock # The C 2050 model is not thread safe

  # This allows users to download the excel spreadsheet version of the model
  get '/model.xlsx' do
    send_file 'model/model.xlsx'
  end

  post '/submit-pathway' do
    content_type :json
    res = Net::HTTP.post_form(
      URI.parse('https://www.google.com/recaptcha/api/siteverify'),
      {
        'secret' => '6LdE5QUTAAAAALlbxcHHu1-Vz-_-I-xSsHCNEtYp',
        'remoteip'   => "127.0.0.1",
        'response'   => params["g-recaptcha-response"]
      }
    )
    success = JSON.parse(res.body)["success"]
    if success
      ObelusMail.send_pathway(params["pathway"]) 
      { success: success, message: 'Data pathway sudah di submit ! terima kasih' }.to_json
    else
      { success: success, message: 'Data pathway gagal dikirim !' }.to_json
    end
  end

  # This has the methods needed to dynamically create the view
  if development?

    helpers Helper
    set :views, settings.root 
    
    # This is the main method for getting data
    get '/pathways/:id/data' do |id|
      DataFromModel.new.calculate_pathway(id).to_json
    end

    # Landing Page
    get '/' do 
      erb :'landing.html'
    end

    get '*' do
      erb :'index.html'
    end
  else

    # This is the main method for getting data
    get '/pathways/:id/data' do |id|
      last_modified Model.last_modified_date # Don't bother recalculating unless the model has changed
      expires (24*60*60), :public # cache for 1 day
      content_type :json # We return json
      DataFromModel.new.calculate_pathway(id).to_json
    end

    get '*' do 
      send_file 'public/index.html'
    end
    
    # Landing Page
    get '/' do 
      send_file 'public/landing.html'
    end

  end

end
