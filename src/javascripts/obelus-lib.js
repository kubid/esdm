JSON.flatten = function(data) {
    var result = {};
    function recurse (cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
             for(var i=0, l=cur.length; i<l; i++)
                 recurse(cur[i], prop + "[" + i + "]");
            if (l == 0)
                result[prop] = [];
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop+"."+p : p);
            }
            if (isEmpty && prop)
                result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
}


obelusMaxValueOfArrayNumber = function(obj){
    flat_obj = JSON.flatten(obj)
    function isFloat(n) {
        return n === +n && n !== (n|0);
    }
    ary = new Array;
    for(var o in flat_obj) {
        ary.push(flat_obj[o]);
    }
    var filtered = ary.filter(isFloat);
    max_number = Math.max.apply(Math, filtered)
    return max_number
}

$(document).ready(function(){
    $('#float-menu-container').draggable()
})