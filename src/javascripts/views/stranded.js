// obelus-c3-chart
window.twentyfifty.views.stranded = function() {

  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity, #cost-macro-micro').hide()
  }
  this.teardown = function(){}

  this.updateResults = function(pathway){
    pasokan_ary  = pathway.stranded_pasokan;
    kapasitas_ary  = pathway.stranded_kapasitas;
    capex_ary     = pathway.stranded_capex;

    $('.ob-chart').show();

    max1 = obelusMaxValueOfArrayNumber(pasokan_ary)
    max2 = obelusMaxValueOfArrayNumber(kapasitas_ary)
    max3 = obelusMaxValueOfArrayNumber(capex_ary)
    //max3 = 1000
    max_val = Math.max(max1, max2)

    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('stranded_pasokan', 'Pasokan batubara', pasokan_ary, 'area', max1)
      chart.updateStacked('stranded_kapasitas', 'Kapasitas Pembangkit Batubara', kapasitas_ary, 'bar', max2)
      chart.updateStacked('stranded_capex', 'CAPEX Penambahan Pembangkit Batubara',capex_ary, 'bar', max3)
    }else{
      chart = new obelusChart();
      chart.stacked('stranded_pasokan', 'Pasokan batubara', pasokan_ary, '#ob-chart1', 'area', 'TWh/yr', max1)
      chart.stacked('stranded_kapasitas', 'Kapasitas Pembangkit Batubara', kapasitas_ary, '#ob-chart2', 'bar', 'GW', max2)
      chart.stacked('stranded_capex', 'CAPEX Penambahan Pembangkit Batubara',capex_ary,'#ob-chart3', 'bar', 'Trilliun Rp', max3)
    }
  }
  
  return this;
}.call({});
