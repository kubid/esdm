// obelus-c3-chart
window.twentyfifty.views.biaya = function() {

  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity, #cost-macro-micro').hide()
  }
  this.teardown = function(){}

  this.updateResults = function(pathway){
    biayacapex_ary  = pathway.biaya_capex;
    biayacapexkumulatif_ary    = pathway.biaya_emisi;
    enland_ary  = pathway.biaya_pes;

    $('.ob-chart').show();
    max1 = obelusMaxValueOfArrayNumber(biayacapex_ary)
    //max2 = obelusMaxValueOfArrayNumber(biayacapexkumulatif_ary)
    //max1 = 8000
    max2 = obelusMaxValueOfArrayNumber(biayacapexkumulatif_ary)
    //max3 = 1500
    max3 = obelusMaxValueOfArrayNumber(enland_ary)
    //max_val = Math.max(max1, max2)

    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('biaya_capex','CAPEX & OPEX Total',biayacapex_ary, 'bar', max1)
      chart.updateStacked('biaya_emisi','Biaya Emisi',biayacapexkumulatif_ary, 'bar', max2)
      chart.updateStacked('biaya_pes','PES', enland_ary, 'bar', max3)
    }else{
      chart = new obelusChart();
      //chart.combination_2('biaya_capex','CAPEX & OPEX Total',biayacapex_ary,'#ob-chart1', 'bar', 'Trilliun Rp/yr')
      chart.stacked('biaya_capex','CAPEX & OPEX Total',biayacapex_ary,'#ob-chart1', 'bar', 'Trilliun Rp/yr', max1)
      chart.stacked('biaya_emisi','Biaya Emisi',biayacapexkumulatif_ary,   '#ob-chart2', 'bar', 'Trilliun RP/yr', max2)
      chart.stacked('biaya_pes','PES', enland_ary, '#ob-chart3', 'bar', 'Trilliun Rp/yr', max3)
    }
  }
  return this;
}.call({});
