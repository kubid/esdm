window.twentyfifty.views.sankey = function() {
    var xyear = null;
    name_conversions = {
        "UK land based bioenergy": "Forests & biocrops",
        "Bio-conversion": "Biomass processing",
        "H2 conversion": "Hydrogen production",
        "H2": "Hydrogen",
        "Useful district heat": "Delivered heat",
        "Heating and cooling - homes": "Home heating & cooling",
        "Heating and cooling - commercial": "Office heating & cooling",
        "Lighting & appliances - homes": "Home lighting & appliances",
        "Lighting & appliances - commercial": "Office lighting & appliances"
    };

    convert_name = function(name) {
        return name_conversions[name] || name;
    };
    this.pillElements = function(pathway) {
        years = pathway.sankey[0].slice()
        years.splice(0, 2)
        ary = []
        $.each(years, function(i, year) {
            active_class = ''
            if (year == 2050) {
                active_class = 'active'
            }
            str = '<li role="presentation"><a href="#" class="view ' + active_class + '"  data-view="sankey" data-year="' + year + '">' + year + '</></li>'
            ary.push(str)
        })
        str = '<ul class="nav nav-pills" id="sankey-nav">' + ary.join("") + '</ul>'
        return str
    }

    this.updateResults = function(pathway) {
        $('#ob-chart-container, #ob-mini-paper, .ob-chart').hide()

        // Expects the flow table to be in the form of
        // [
        // ["From", "To", 2007, 2010, ..., 2050],
        // ["BatubaraReserves", "Coal", 124, 128, ..., 64],
        // ...
        // ]
        if ($('#sankey-nav').length < 1) {
            $('#results').prepend(this.pillElements(pathway))
            sankeyNav();
        }
        $('#results').children('div[id!="sankey"]').remove()
        $('#results').children('#sankey').not(':first').remove()
        $('#results').children('#sankey').children().not(':first').remove()
        selected_year = '' + $('#sankey-nav li a.active').data('year')

        // Look for the indices of the columns we want:
        header = pathway.sankey[0];
        from_column = header.indexOf("From");
        to_column = header.indexOf("To");
        flow_column = header.indexOf(parseInt(selected_year)); // We only care about 2050 data at the moment

        // Check the table is ok
        if (from_column == -1 || to_column == -1 || flow_column == -1) {
            console.log("Energy flow table in unexpected format");
        }

        // Turn it into the form that the sankey library requires:
        // [[from, flow, to]]
        data = pathway.sankey.slice(1).map(function(row) { // slice(1) to skip header row
            return [row[from_column], row[flow_column], row[to_column]]
        });

        // Auto adjust sankey container height - rifki
        // years = []
        //   $.each($('#sankey-nav li a'), function(i,e){
        //   years.push($(e).text())
        // })
        // years.sort(function(a, b){return b-a})
        // idx = years.indexOf(selected_year)  

        // multiplier = 0.02 + parseFloat('0.0'+idx)
        // this.s.convert_flow_values_callback = function(flow) {
        //     return flow * multiplier;
        // };


        this.s.updateData(data);
        this.s.redraw();
        max_y = s.boxes["Produksi bioenergi"].b();
        if ($('#sankey').height() < max_y) {
            $('#sankey').height(max_y);
              this.s.r.setSize($('#sankey').width(), max_y);
        }
    };

    this.teardown = function() {
        $('#results').empty();
        s = null;
    };

    this.setup = function() {
        $('#results').append("<div id='sankey'></div>");
        this.s = s = new Sankey();
	s.stack(0,["Hidro", "Surya", "Bayu", "OTEC", "Produksi minyak bumi", "Impor minyak bumi", "Produksi gas bumi", "Impor gas bumi", "Produksi batubara", "Impor batubara", "Pertanian dan perkebunan", "Limbah"]);
	s.stack(1,["Minyak bumi"], "Produksi minyak bumi");
	s.stack(1,["Gas bumi"], "Produksi gas bumi");
	s.stack(1,["Batubara"], "Impor batubara");
	s.stack(1,["Produksi bioenergi"], "Limbah");
	s.stack(2,["Cair", "Gas", "Padat"], "Minyak bumi");
	s.stack(3,["Pembangkit lainnya"], "Hidro");
	s.stack(3,["Pembangkit thermal"], "Padat");
	s.stack(4,["Jaringan listrik"], "Pembangkit thermal");
	s.stack(5,["R. tangga - penerangan", "R. tangga - pendinginan dan pemanasan", "R. tangga - memasak", "R. tangga - lainnya", "Komersial - penerangan", "Komersial - pendinginan dan pemanasan", "Komersial - memasak", "Komersial - lainnya", "Industri - skala besar", "Industri - skala menengah dan kecil", "Penumpang - dalam kota", "Penumpang - antar kota", "Angkutan barang", "Angkutan udara", "Pertambangan mineral", "Pertanian dan konstruksi", "Kelebihan/ekspor", "Rugi-rugi"]);


    	//s.nudge_boxes_callback = function() {
        //    this.boxes["Rugi-rugi"].y = this.boxes["Pertanian dan konstruksi"].b() - this.boxes["Rugi-rugi"].size();
        //};

        s.setColors({
            "Produksi batubara": "#434443",
            "Impor batubara": "#434443",
            "Produksi minyak bumi": "#A99268",
            "Impor minyak bumi": "#A99268",
            "Produksi gas bumi": "#3e96cc",
            "Impor gas bumi": "#3e96cc",
	    "Batubara": "#8E8E8E",
	    "Padat": "#3D5C38",
	    "Gas": "#3e96cc",
	    "Cair": "#baa786",
	    "Minyak bumi": "#BAA786",
	    "Gas bumi": "#3e96cc",
            "Surya": "#1C9900",
            "Hidro": "#1C9900",
	    "Bayu": "#1C9900",
	    "OTEC": "#1C9900",
            "Produksi bioenergi": "#1C9900",
	    "Limbah": "#1C9900",
	    "Pertanian dan perkebunan": "#1C9900",
	    "Pembangkit thermal": "#F1EA50",
	    "Pembangkit lainnya": "#F1EA50",
	    "Jaringan listrik":  "#F4C502",
            "H2": "#FF6FCF"
        });

        s.nudge_colours_callback = function() {
            //this.recolour(this.boxes["Padat"].left_lines, "#557731");
            //this.recolour(this.boxes["Gas"].left_lines, "#3e96cc");
            this.recolour(this.boxes["Rugi-rugi"].left_lines, "#ddd");
            //this.recolour(this.boxes["Jaringan listrik"].left_lines,  "#EAE008");
        };

        pixels_per_TWh = $('#sankey').height() / 1;

        s.y_space = Math.round(0.03 * pixels_per_TWh);
        s.right_margin = 200;
        s.left_margin = 200;
        
        s.convert_flow_values_callback = function(flow) {          
            return flow * 0.001;
        };

        s.convert_flow_labels_callback = function(flow) {
            return Math.round(flow);
        };

        s.convert_box_value_labels_callback = function(flow) {
            return "" + Math.round(flow) + " TWh/y";
        };
    };
    return this;
}.call({});
