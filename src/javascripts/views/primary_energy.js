// obelus-c3-chart
window.twentyfifty.views.primary_energy_chart = function() {
  
  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity, #cost-macro-micro').hide()
  }
  this.teardown = function(){}

  this.updateResults = function(pathway, cached){
    demand_ary  = pathway.final_energy_demand;
    supply_ary  = pathway.primary_energy_supply;
    selisih_ary = pathway.importdependency;
    
    $('.ob-chart').show();
    max1 = obelusMaxValueOfArrayNumber(demand_ary)
    max2 = obelusMaxValueOfArrayNumber(supply_ary)
    max_val = Math.max(max1, max2)
    max_val = max2
    
    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('final_energy_demand', 'Permintaan Energi Final', demand_ary, 'area', max_val)
      chart.updateStacked('primary_energy_supply', 'Pasokan Energi Primer',   supply_ary, 'area', max_val)
      chart.updateStacked('importdependency','Ketergantungan Impor',selisih_ary, 'line', null)
    }else{
      chart = new obelusChart();
      chart.stacked('final_energy_demand', 'Permintaan Energi Final', demand_ary,  '#ob-chart1', 'area', 'TWh/yr', max_val)
      chart.stacked('primary_energy_supply', 'Pasokan Energi Primer',   supply_ary,  '#ob-chart2', 'area', 'TWh/yr', max_val)
      chart.stacked('importdependency','Ketergantungan Impor',    selisih_ary, '#ob-chart3', 'line', '%')
    }
  }

  return this;
}.call({});
