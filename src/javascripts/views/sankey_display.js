window.twentyfifty.views.sankey = function() {
    var xyear = null;
    name_conversions = {
      "UK land based bioenergy": "Forests & biocrops",
			"RT Penerangan": "Rumah tangga - Penerangan",
			"RT Pendinginan":  "Rumah tangga - Pendinginan",
			"RT Memasak": "Rumah tangga - Memasak",
			"RT Lainnya": "Rumah tangga - Lainnya",
			"PKP": "Pertanian, kontruksi dan pertambangan",
      "Bio-conversion": "Biomass processing",
      "H2 conversion": "Hydrogen production",
      "H2": "Hydrogen",
      "Useful district heat": "Delivered heat",
      "Heating and cooling - homes": "Home heating & cooling",
      "Heating and cooling - commercial": "Office heating & cooling",
      "Lighting & appliances - homes": "Home lighting & appliances",
      "Lighting & appliances - commercial": "Office lighting & appliances"
    };

    function formatNumber (num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    };

    convert_name = function(name) {
        return name_conversions[name] || name;
    };
    this.pillElements = function(pathway) {
        years = pathway.sankey[0].slice()
        years.splice(0, 2)
        ary = []
        $.each(years, function(i, year) {
            active_class = ''
            if (year == 2050) {
                active_class = 'active'
            }
            str = '<li role="presentation"><a href="#" class="view ' + active_class + '"  data-view="sankey" data-year="' + year + '">' + year + '</></li>'
            ary.push(str)
        })
        str = '<ul class="nav nav-pills" id="sankey-nav">' + ary.join("") + '</ul>'
        return str
    }

    this.updateResults = function(pathway) {
        $('#ob-chart-container, #ob-mini-paper, .ob-chart').hide()

        // Expects the flow table to be in the form of
        // [
        // ["From", "To", 2007, 2010, ..., 2050],
        // ["BatubaraReserves", "Coal", 124, 128, ..., 64],
        // ...
        // ]
        if ($('#sankey-nav').length < 1) {
            $('#results').prepend(this.pillElements(pathway))
            sankeyNav();
        }
        $('#results').children('div[id!="sankey"]').remove()
        $('#results').children('#sankey').not(':first').remove()
        $('#results').children('#sankey').children().not(':first').remove()
        selected_year = '' + $('#sankey-nav li a.active').data('year')

        // Look for the indices of the columns we want:
        header = pathway.sankey[0];
        from_column = header.indexOf("From");
        to_column = header.indexOf("To");
        flow_column = header.indexOf(parseInt(selected_year)); // We only care about 2050 data at the moment

        // Check the table is ok
        if (from_column == -1 || to_column == -1 || flow_column == -1) {
            console.log("Energy flow table in unexpected format");
        }

        // Turn it into the form that the sankey library requires:
        // [[from, flow, to]]
        data = pathway.sankey.slice(1).map(function(row) { // slice(1) to skip header row
            return [row[from_column], row[flow_column], row[to_column]]
        });

        // Auto adjust sankey container height - rifki
        // years = []
        //   $.each($('#sankey-nav li a'), function(i,e){
        //   years.push($(e).text())
        // })
        // years.sort(function(a, b){return b-a})
        // idx = years.indexOf(selected_year)  

        // multiplier = 0.02 + parseFloat('0.0'+idx)
        // this.s.convert_flow_values_callback = function(flow) {
        //     return flow * multiplier;
        // };


        this.s.updateData(data);
        this.s.redraw();
        max_y = s.boxes['Produksi bioenergi'].b();
	console.log('-----Max Y Sankey----' + max_y)
        
            $('#sankey').height(max_y);
            //  this.s.r.setSize($('#sankey').width(), max_y);
        
    };

    this.teardown = function() {
        $('#results').empty();
        s = null;
    };

    this.setup = function() {
        $('#results').append("<div id='sankey'></div>");
        this.s = s = new Sankey();
	s.stack(0, ["Hidro","Surya", "Angin","Energi laut", "Nuklir", "Panas bumi", "Produksi minyak bumi", "Impor minyak bumi","Produksi gas bumi", "Impor gas bumi", "Produksi batubara", "Impor batubara","Pertanian dan perkebunan","Limbah"]);
	s.stack(1, ["Minyak bumi"], "Impor minyak bumi");
	s.stack(1, ["Gas bumi"], "Produksi gas bumi");
	s.stack(1, ["Batubara"], "Produksi batubara");
	s.stack(1, ["Produksi bioenergi"], "Limbah");
	s.stack(2, ["Kilang"], "Minyak bumi");
	s.stack(3, ["Cair", "Gas", "Padat"], "Minyak bumi");
	s.stack(4, ["Pembangkit lainnya"], "Hidro");
	s.stack(4, ["Pembangkit thermal"], "Padat");
	s.stack(5, ["Jaringan listrik"], "Cair");
	s.stack(6, ["Kelebihan/ekspor"], "Produksi bioenergi");
	s.stack(6, ["Rumah tangga", "Komersial", "Industri", "Transportasi barang", "Transportasi penumpang", "Transportasi udara", "Pertanian, kontruksi dan pertambangan",  "Rugi-rugi"]);
	
        s.nudge_boxes_callback = function() {
            this.boxes["Kelebihan/ekspor"].y = this.boxes["Limbah"].b() - this.boxes["Kelebihan/ekspor"].size();
        };

        s.setColors({
					"Produksi batubara": '#604343',
          "Impor batubara": '#604343',
          "Produksi minyak bumi": '#111E06',
          "Impor minyak bumi": '#111E06',
         	"Produksi gas bumi": '#3e96cc',
         	"Impor gas bumi": '#3e96cc',
         	"Limbah": "#1C9900",
		    	"Nuklir": "#1C9900",
					"Hidro": "#1C9900",
					"Surya": "#1C9900",
					"Angin": "#1C9900",
					"Panas bumi": "#1C9900",
					"Energi laut": "#1C9900",
         	"Padat": "#7f6868",
         	"Cair": "#7D9763",
         	"Gas": "#64abd6",
					"Pembangkit thermal": "#BCC2AD",
					"Pembangkit lainnya": "#1C9900",
					"Kilang": "#060B02",
					"Jaringan listrik": "#f9fb00",
					"Batubara": "#000000",
          "H2": "#FF6FCF"
        });

        s.nudge_colours_callback = function() {
            this.recolour(this.boxes["Padat"].left_lines, '#604343');
            this.recolour(this.boxes["Gas"].left_lines, "#3e96cc");
				    this.recolour(this.boxes["Cair"].left_lines, '#111E06');
            this.recolour(this.boxes["Rugi-rugi"].left_lines, "#ddd");
				    this.recolour(this.boxes["Produksi bioenergi"].left_lines, "#1C9900");
				    this.recolour(this.boxes["Kilang"].left_lines, "#060B02");
				    this.recolour(this.boxes["Kelebihan/ekspor"].left_lines, "#f76352");
            this.recolour(this.boxes["Jaringan listrik"].left_lines, "#fade38");
        };

        pixels_per_TWh = $('#sankey').height() / 50000;
        s.y_space = Math.round(950 * pixels_per_TWh);
        s.right_margin = 150;
        s.left_margin = 130;
        
        s.convert_flow_values_callback = function(flow) {          
            return flow * 0.009;
        };

        s.convert_flow_labels_callback = function(flow) {
            return formatNumber(flow.toFixed(2));
        };

        s.convert_box_value_labels_callback = function(flow) {
            return "" + formatNumber(flow.toFixed(2)) + " TWh/y";
        };
    };
    return this;
}.call({});
