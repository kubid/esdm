// obelus-c3-chart
window.twentyfifty.views.cost_makro_mikro = function() {
  this.elements = {container_table: 'cmm1', container_chart_1: 'cmm2', container_chart_2: 'cmm3'}

  this.setup    = function(){
    $('#ob-chart-container, #ob-mini-paper').hide()
    return $('#results').append("<div id='cost-macro-micro'><div id='"+this.elements.container_table+"' class='column'></div><div id='"+this.elements.container_chart_1+"' class='column'></div><div id='"+this.elements.container_chart_2+"' class='column'></div><div class='clear'></div></div>");
  }
  this.teardown = function() {
      this.ready = false;
      return $('#results').empty();
    };

  this.updateResults = function(pathway){
    this.pathway = pathway;
    this.updatedColumn1();

    stranded_ary = pathway.stranded_npv;
    kapasitas_ary  = pathway.stranded_kapasitas;
    capex_ary     = pathway.stranded_timeseries;

    max2 = obelusMaxValueOfArrayNumber(stranded_ary)
    max3 = obelusMaxValueOfArrayNumber(capex_ary)
    //max3 = 1000
    max_val = Math.max(max2, max3)
    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('stranded_kapasitas', 'Total NPV Biaya Terdampar vs. CAPEX PLTU Batubara', stranded_ary, 'bar', max2)
      chart.updateStacked('stranded_capex', 'NPV per Periode',capex_ary, 'bar', max3)
    }else{
      chart = new obelusChart();
      chart.stacked('stranded_kapasitas', 'Total NPV Biaya Terdampar vs. CAPEX PLTU Batubara', stranded_ary, '#'+this.elements.container_chart_1, 'bar', 'Trilliun Rp', max2)
      chart.stacked('stranded_capex', 'NPV per Periode',capex_ary,'#'+this.elements.container_chart_2, 'bar', 'Trilliun Rp', max3)
    }
  }  

  function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
   };

  function sortNumber(a,b) {
    return b-a;
  }

  this.updatedColumn1 = function() {
    var element, name, values, _ref;
    sector_1 = "sector_1"
    sector_2 = "sector_2"
    element = $('#cmm1');
    element.empty();
    element.append("<h2>Analisa Biaya Terdampar</h2>");
    element.append("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum neque purus, cursus sed justo sit amet, porttitor convallis nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam nec dapibus massa. Aliquam consectetur mattis mauris venenatis rutrum. Vivamus ullamcorper sollicitudin leo ut consequat. In venenatis lectus eget orci commodo, a varius enim pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin tempus leo sed nisl elementum mollis. Maecenas at orci gravida, bibendum eros quis, aliquet sapien. Nunc volutpat quam faucibus tincidunt convallis. Aenean dignissim, nunc vel auctor consequat, enim leo aliquam tellus, nec aliquet elit nunc et leo. Mauris egestas turpis mauris, non tempor nulla fringilla eget. Curabitur eu posuere ex. Curabitur blandit leo magna, id ullamcorper nisi maximus a. Phasellus efficitur sodales metus ac ornare.</p>");
    element.append("<table class='cmm1 ob-table-2'>");
    elem = $('table.cmm1')
    elem.append("<tr class='top-title'><th class='description'></th><th class='year'>NPV @2014</th></tr>");
    elem.append("<tr class='title'><th class='description'>Perusahaan</th><th class='value'>Trilliun Rp</th></tr>");
    _ref = this.pathway.stranded_bycompany;
    idx_2011 = 1, idx_2050 = 9;
    
    for (name in _ref) {
    
      if (!__hasProp.call(_ref, name)) continue;
      values = _ref[name];
      if(name != "0"){
        elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td></tr>");  
      }
    }
    sorted_data = this.sortData()
    $('.text1').text(sorted_data[0])
    $('.text2').text(sorted_data[1])
  };

   this.sortData = function(){
    data_values = []
    data_descriptions = []

    $('#cmm1 td.description').each(function(){
      data_descriptions.push($(this).text())
    })

    $('#cmm1 td.value').each(function(i,e){
      value = $(this).text().replace(",", '')
      if(i%2 == 1){
        data_values.push(parseFloat(value))
      }
    })

    data_object = {}
    $(data_values).each(function(i,e){
      data_object[data_values[i]] = data_descriptions[i]
    })
    data_values = data_values.sort(sortNumber);    

    sorted = []
    sorted.push(data_object[data_values[1]])
    sorted.push(data_object[data_values[2]])
    return sorted
  }

  return this;
}.call({});
