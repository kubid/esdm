// obelus-c3-chart
window.twentyfifty.views.biouse = function() {

  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity, #cost-macro-micro').hide()
  }
  this.teardown = function(){}

  this.updateResults = function(pathway){
    cairan_ary  = pathway.bio_cairan;
    padatan_ary    = pathway.bio_padatan;
    gas_ary  = pathway.bio_gas;

    $('.ob-chart').show();
    max1 = obelusMaxValueOfArrayNumber(cairan_ary)
    max2 = obelusMaxValueOfArrayNumber(padatan_ary)
    //max2 = 15000
    max3 = obelusMaxValueOfArrayNumber(gas_ary)
    max_val = Math.max(max1, max2, max3)
    console.log('-------Max value-----:' + max_val)

    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('bio_cairan','Hidrokarbon Cair', cairan_ary, 'bar', null)
      chart.updateStacked('bio_padatan','Hidrokarbon Padat', padatan_ary, 'bar', null)
      chart.updateStacked('bio_gas','Hidrokarbon Gas', gas_ary, 'bar', null)
    }else{
      chart = new obelusChart();
      chart.stacked('bio_cairan','Hidrokarbon Cair', cairan_ary,'#ob-chart1', 'bar', 'TWh/yr', max_val)
      //chart.stacked('biaya_capex','CAPEX & OPEX Total',biayacapex_ary,'#ob-chart1', 'bar', 'Trilliun Rp/yr', max1)
      chart.stacked('bio_padatan','Hidrokarbon Padat', padatan_ary,   '#ob-chart2', 'bar', 'TWh/yr', max_val)
      chart.stacked('bio_gas','Hidrokarbon Gas', gas_ary, '#ob-chart3', 'bar', 'TWh/yr', max_val)
    }
  }
  return this;
}.call({});
