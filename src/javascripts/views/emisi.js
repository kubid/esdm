// obelus-c3-chart
window.twentyfifty.views.emisi = function() {

  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity, #cost-macro-micro').hide()
  }
  this.teardown = function(){}

  this.updateResults = function(pathway){
    energy_ary  = pathway.emission.energy;
    land_ary    = pathway.emission.land;
    enland_ary  = pathway.emission.energy_and_land;

    $('.ob-chart').show();
    max1 = obelusMaxValueOfArrayNumber(energy_ary)
    max2 = obelusMaxValueOfArrayNumber(land_ary)
    // max3 = obelusMaxValueOfArrayNumber(enland_ary)
    max_val = Math.max(max1, max2)

    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('emission.energy','Emisi Energi',energy_ary, 'area', max_val)
      chart.updateStacked('emission.land','Emisi Lahan',land_ary, 'area', max_val)
      chart.updateStacked('emission.energy_and_land','Emisi Energi dan Lahan', enland_ary, 'area', null)
    }else{
      chart = new obelusChart();
      chart.stacked('emission.energy','Emisi Energi',energy_ary,'#ob-chart1', 'area', 'MtCO2e/yr', max_val)
      chart.stacked('emission.land','Emisi Lahan',land_ary,   '#ob-chart2', 'area', 'MtCO2e/yr', max_val)
      chart.stacked('emission.energy_and_land','Emisi Energi dan Lahan', enland_ary, '#ob-chart3', 'area', 'MtCO2e/yr')
    }
  }
  return this;
}.call({});
