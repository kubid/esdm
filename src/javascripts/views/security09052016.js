window.twentyfifty.views.security = function() {
  
      this.setup = function() {
        this.ready = true;
        $('#ob-chart-container, #ob-mini-paper').hide()  
        $('#classic_controls').show()      
        return $('#results').append("<div id='energysecurity'><div id='energy-demand' class='column'></div><div id='energy-supply' class='column'></div><div id='electricity-supply' class='column'></div><div class='clear'></div></div>");
      };

    this.teardown = function() {
      this.ready = false;
      return $('#results').empty();
    };

   function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
   };

    this.updateResults = function(pathway) {
      this.pathway = pathway;
      if (!this.ready) {
        this.setup();
      }
      this.updatedElectricitySupply();
      this.updatedEnergySupply();
      this.updatedEnergyDemand();
    };

    this.updatedElectricitySupply = function() {
      var element;
      element = $('#electricity-supply');
      element.empty();
      element.append("<h2>Pasokan Listrik</h2>");
      element.append("<p>Permintaan listrik yang ada akan dipenuhi oleh sejumlah pembangkit listrik. Pembangkit listrik  tersebut dapat berupa pembangkit berbahan bakar fosil atau energi baru dan terbarukan (EBT). Semakin besar pangsa pembangkit EBT, semakin rendah emisi yang akan dihasilkan dari sektor ketenagalistrikan.</p>");
      element.append("<table class='electricity-supply ob-table-2'>");
      elem = $('table.electricity-supply')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>TWh/yr</th><th class='value'>TWh/yr</th></tr>");
      _ref = this.pathway.electricity.supply;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        // if(name != "0" && values[0] != "Total"){
        if(name != "0" ){
          if(values[idx_2011] == null){
            var var_2011 = 0
          }else{
            var var_2011 = formatNumber(values[idx_2011].toFixed(2))
          }
          if(values[idx_2050] == null){
            var var_2050 = 0
          }else{
            var var_2050 = formatNumber(values[idx_2050].toFixed(2))
          }
          
          elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + var_2011 + "</td><td class='value'>" + var_2050 + "</td></tr>");  
        }
        
      }
    };

    this.updatedEnergySupply = function() {
      var element, name, values, _ref;
      element = $('#energy-supply');
      element.empty();
      element.append("<h2>Pasokan Energi</h2>");
      element.append("<p>Pasokan energi primer dalam tabel ini termasuk rugi-rugi yang ada seperti yang disajikan dalam grafik Pasokan Energi Primer. Pasokan hidrokarbon padat berupa batubara akan mendominasi produksi energi hingga tahun 2050. Hal ini disebabkan potensi batubara yang besar di Indonesia. Di sisi lain, pasokan hidrokarbon cair masih didominasi leh minyak bumi dan sebagian besar berupa impor. </p>");
      element.append("<table class='energy-supply ob-table-2'>");
      elem = $('table.energy-supply')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>TWh/yr</th><th class='value'>TWh/yr</th></tr>");
      _ref = this.pathway.energy_security.supply;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        if(name != "0"){
          elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td><td class='value'>" + formatNumber(values[idx_2050].toFixed(2))+ "</td></tr>");  
        }
        
      }
    };

    this.updatedEnergyDemand = function() {
      var element, name, values, _ref;
      element = $('#energy-demand');
      element.empty();
      element.append("<h2>Permintaan Energi</h2>");
      element.append("<p>Permintaan energi dalam bentuk hidrokarbon cair dan padat masih memdominasi hingga tahun 2050. Sektor transportasi akan menjadi penggerak utama permintaan energi hidrokarbon cair, sedangkan sektor ketenagalistrikan dan industri menjadi penggerak permintaan energi hidrokarbon padat. Keterbatasan pasokan domestik akan menyebabkan peningkatan impor jenis hidrokarbon tersebut, sebagaimana terlihat pada diagram Alur Energi.</p>");
      element.append("<table class='energy-demand ob-table-2'>");
      elem = $('table.energy-demand')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>TWh/yr</th><th class='value'>TWh/yr</th></tr>");
      _ref = this.pathway.energy_security.demand;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        if(name != "0"){
          elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td><td class='value'>" + formatNumber(values[idx_2050].toFixed(2))+ "</td></tr>");  
        }
        
      }
      return element.append("</table>");
    };

  return this;
}.call({});
