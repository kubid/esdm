window.twentyfifty.views.mini_paper = function() {
  
  this.setup    = function(){}
  this.teardown = function(){}

  this.updateResults = function(pathway){
    $('#ob-chart-container, #classic_controls').hide()
    $('#ob-mini-paper').show()
  }

  return this;
}.call({});
