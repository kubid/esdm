// obelus-c3-chart
window.twentyfifty.views.cost_makro_mikro = function() {
  this.elements = {container_table: 'cmm1', container_chart_1: 'cmm2', container_table2: 'cmm3'}

  this.setup    = function(){
    $('#ob-chart-container, #ob-mini-paper').hide()
    return $('#results').append("<div id='cost-macro-micro'><div id='"+this.elements.container_table+"' class='column'></div><div id='"+this.elements.container_chart_1+"' class='column'></div><div id='"+this.elements.container_table2+"' class='column'></div><div class='clear'></div></div>");
  }
  this.teardown = function() {
      this.ready = false;
      return $('#results').empty();
    };

  this.updateResults = function(pathway){
    this.pathway = pathway;
    this.updatedColumn1();
    this.updatedColumn2();

    stranded_ary = pathway.stranded_npv;
    kapasitas_ary  = pathway.stranded_kapasitas;
    capex_ary     = pathway.stranded_timeseries;

    max2 = obelusMaxValueOfArrayNumber(stranded_ary)
    max3 = obelusMaxValueOfArrayNumber(capex_ary)
    //max3 = 1000
    max_val = Math.max(max2, max3)
    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('stranded_kapasitas', 'Total NPV Nilai Aset Terdampar vs. CAPEX PLTU Batubara', stranded_ary, 'bar', max2)
      chart.updateStacked('stranded_capex', 'NPV per Periode',capex_ary, 'bar', max3)
    }else{
      chart = new obelusChart();
      chart.stacked('stranded_kapasitas', 'Total NPV Nilai Terdampar vs. CAPEX PLTU Batubara', stranded_ary, '#'+this.elements.container_chart_1, 'bar', 'Trilliun Rp', max2)
      chart.stacked('stranded_capex', 'NPV per Periode',capex_ary,'#'+this.elements.container_chart_2, 'bar', 'Trilliun Rp', max3)
    }
  }  

  function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
   };

  function sortNumber(a,b) {
    return b-a;
  }

  this.updatedColumn1 = function() {
    var element, name, values, _ref;
    sector_1 = "sector_1"
    sector_2 = "sector_2"
    element = $('#cmm1');
    element.empty();
    element.append("<h3>Analisa Nilai Terdampar</h3>");
    element.append("<p>Aset terdampar atau stranded asset merupakan aset yang nilainya hilang, menurun atau berubah menjadi kewajiban (<i>liability</i>) karena penyebab tertentu. Salah satunya terkait dengan perubahan iklim. Misalnya, penyebab yang diakibatkan oleh kebijakan penurunan emisi gas rumah kaca atau penyebab yang dipicu oleh perubahan kesadaran konsumen terhadap dampak bahan bakar fosil. Di antara bahan bakar fosil tersebut, batubara merupakan sumber daya yang tergolong mengandung intensitas karbon paling tinggi.</p>");
    element.append("<p>Nilai dari aset terdampar dapat diperoleh dengan cara membandingkan nilai yang diharapkan (misalnya oleh perusahaan pertambangan batubara atau perusahaan pembangkit tenaga listrik yang menggunakan batubara) dengan nilai yang muncul ketika permintaan atas sumber daya batubara berubah karena penyebab terkait perubahan iklim di atas. Hal ini ditunjukkan dengan turunnya harga batubara. Dalam keadaan demikian, dari sisi pasokan, cadangan batubara atau pembangkit listrik berbasis batubara secara ekonomi tidak lagi menguntungkan untuk ditambang atau untuk digunakan. Aset berupa cadangan batubara atau fasilitas pembangkit pun menjadi terdampar.</p>");
    element.append("<p>Sebagai ilustrasi, nilai aset terdampar yang dianalisis di dalam <i>Indonesia 2050 Pathway Calculator</i> (I2050PC) menggunakan contoh hulu dan hilir. Untuk contoh hulu, aset terdampar dihitung untuk kasus pertambangan batubara, terutama aset batubara yang terdampar dari perusahaan-perusahaan tambang batubara besar dengan mempertimbangkan perubahan harga, nilai waktu (<i>net present value</i>, NPV), dan umur tambang batubara. Adapun untuk contoh hilir, aset terdampar ditampilkan dalam bentuk kemungkinan aset pembangkit listrik tenaga batubara mengalami pendamparan (<i>stranding</i>) yang dapat dilihat dari besaran dana yang dikeluarkan sebagai <i>capital expenditure</i> (CAPEX) dari fasilitas pembangkit tersebut.</p>");
    element.append("<table class='cmm1 ob-table-2'>");

  }

 this.updatedColumn2 = function() {
    var element, name, values, _ref;
    sector_1 = "sector_1"
    sector_2 = "sector_2"
    element = $('#cmm3');
    element.empty();
    element.append("<h3>Perkiraan Nilai Aset Terdampar per Perusahaan</h3>");
    element.append("<table class='cmm3 ob-table-2'>");
    elem = $('table.cmm3')
    elem.append("<tr class='top-title'><th class='description'></th><th class='year'>NPV @2014</th></tr>");
    elem.append("<tr class='title'><th class='description'>Perusahaan</th><th class='value'>Trilliun Rp</th></tr>");
    _ref = this.pathway.stranded_bycompany;
    idx_2011 = 1, idx_2050 = 9;
    
    for (name in _ref) {
    
      if (!__hasProp.call(_ref, name)) continue;
      values = _ref[name];
      if(name != "0"){
        elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td></tr>");  
      }
    }
    sorted_data = this.sortData()
    $('.text1').text(sorted_data[0])
    $('.text2').text(sorted_data[1])
  };

   this.sortData = function(){
    data_values = []
    data_descriptions = []

    $('#cmm1 td.description').each(function(){
      data_descriptions.push($(this).text())
    })

    $('#cmm3 td.value').each(function(i,e){
      value = $(this).text().replace(",", '')
      if(i%2 == 1){
        data_values.push(parseFloat(value))
      }
    })

    data_object = {}
    $(data_values).each(function(i,e){
      data_object[data_values[i]] = data_descriptions[i]
    })
    data_values = data_values.sort(sortNumber);    

    sorted = []
    sorted.push(data_object[data_values[1]])
    sorted.push(data_object[data_values[2]])
    return sorted
  }
  return this;
}.call({});
