window.twentyfifty.views.security = function() {
  
      this.setup = function() {
        this.ready = true;
        $('#ob-chart-container, #ob-mini-paper').hide()  
        $('#classic_controls').show()      
        return $('#results').append("<div id='energysecurity'><div id='energy-demand' class='column'></div><div id='energy-supply' class='column'></div><div id='electricity-supply' class='column'></div><div class='clear'></div></div>");
      };

    this.teardown = function() {
      this.ready = false;
      return $('#results').empty();
    };

   function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
   };

    function sortNumber(a,b) {
      return b-a;
    }

    this.updateResults = function(pathway) {
      this.pathway = pathway;
      if (!this.ready) {
        this.setup();
      }
      this.updatedElectricitySupply();
      this.updatedEnergySupply();
      this.updatedEnergyDemand();
    };

    this.updatedElectricitySupply = function() {
      var element, _ref2;
      
      _ref2 = this.pathway.tabel_security.electricityshare;
      
      elec_1 = _ref2[0];
      elec_2 = _ref2[1];
      
      element = $('#electricity-supply');
      element.empty();
      element.append("<h3>Pasokan Listrik</h3>");
      element.append("<p>Pembangkitan listrik dapat berasal dari permbangkit berbasis fosil dan energi baru dan terbarukan (EBT). Semakin tinggi pangsa pembangkit EBT, maka semakin rendah emisi GRK yang ditimbulkan dari sektor ini. Pada skenario ini, pangsa <span class='text-primary'><em>" + elec_1 + "</em></span> adalah yang terbesar, yaitu setara dengan <span class='text-primary'><em>" + formatNumber((elec_2*100).toFixed(2)) + "%</em></span> dari total pembangkitan yang ada.</p>");
      element.append("<table class='electricity-supply ob-table-2'>");
      elem = $('table.electricity-supply')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>TWh/yr</th><th class='value'>TWh/yr</th></tr>");
      _ref = this.pathway.electricity.supply;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        // if(name != "0" && values[0] != "Total"){
        if(name != "0" ){
          if(values[idx_2011] == null){
            var var_2011 = 0
          }else{
            var var_2011 = formatNumber(values[idx_2011].toFixed(2))
          }
          if(values[idx_2050] == null){
            var var_2050 = 0
          }else{
            var var_2050 = formatNumber(values[idx_2050].toFixed(2))
          }
          
          elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + var_2011 + "</td><td class='value'>" + var_2050 + "</td></tr>");  
        }
        
      }
      sorted_data = this.sortData('.column table.electricity-supply')
    };

    this.updatedEnergySupply = function() {
      var element, name, values, _ref;
      element = $('#energy-supply');
      element.empty();
      element.append("<h3>Pasokan Energi</h3>");
      element.append("<p><span class='text-primary'><em><span class='text1'></em></span></span>  dan <span class='text-primary'><span class='text-lowercase'><em><span class='text2'></em></span></span></span> akan menjadi sumber pasokan utama energi pada tahun 2050. </p>");
      element.append("<table class='energy-supply ob-table-2'>");
      elem = $('table.energy-supply')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>TWh/yr</th><th class='value'>TWh/yr</th></tr>");
      _ref = this.pathway.energy_security.supply;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        if(name != "0"){
          elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td><td class='value'>" + formatNumber(values[idx_2050].toFixed(2))+ "</td></tr>");  
        }
        
      }
      sorted_data = this.sortData('.column table.energy-supply')
    };

    this.updatedEnergyDemand = function() {
      var element, name, values, _ref, _ref2, _ref3;
      _ref2 = this.pathway.tabel_security.demandsector;
      _ref3 = this.pathway.tabel_security.demandenergy;
      
      demand_1 = _ref2[0];
      demand_2 = _ref2[1];
      
      energy_1 = _ref3[0];
      energy_2 = _ref3[1];
      
      element = $('#energy-demand');
      element.empty();
      element.append("<h3>Permintaan Energi</h3>");
      element.append("<p>Permintaan energi pada tahun 2050 akan didominasi oleh permintaan <span class='text-primary'><span class='text-lowercase'><em>" + energy_1 + "</em></span></span> dan <span class='text-primary'><span class='text-lowercase'><em>" + energy_2 + "</em></span></span>. Permintaan <span class='text-primary'><span class='text-lowercase'><em>" + energy_1 + "</em></span></span> didorong permintaan dari sektor <span class='text-primary'><span class='text-lowercase'><em>" + demand_1 + "</em></span></span>, sedangkan sektor <span class='text-primary'><span class='text-lowercase'><em>" + demand_2 + "</em></span></span> merupakan pengguna terbesar <span class='text-primary'><span class='text-lowercase'><em>" + energy_2 + "</em></span></span>.</p>");
      element.append("<table class='energy-demand ob-table-2'>");
      elem = $('table.energy-demand')
      elem.append("<tr class='top-title'><th class='description'></th><th class='year'>2011</th><th class='year'>2050</th></tr>");
      elem.append("<tr class='title'><th class='description'>Pasokan</th><th class='value'>TWh/yr</th><th class='value'>TWh/yr</th></tr>");
      _ref = this.pathway.energy_security.demand;
      idx_2011 = 1, idx_2050 = 9;
      
      for (name in _ref) {
      
        if (!__hasProp.call(_ref, name)) continue;
        values = _ref[name];
        if(name != "0"){
          elem.append("<tr><td class='description'>" + values[0] + "</td><td class='value'>" + formatNumber(values[idx_2011].toFixed(2))+ "</td><td class='value'>" + formatNumber(values[idx_2050].toFixed(2))+ "</td></tr>");  
        }
        
      }
      sorted_data = this.sortData('.column table.energy-demand')      
      return element.append("</table>");
    };

    this.sortData = function(table_el){
      data_values = []
      data_descriptions = []
      
      $(table_el+' td.description').each(function(){
        data_descriptions.push($(this).text())
      })

      $(table_el+' td.value').each(function(i,e){
        value = $(this).text().replace(",", '')
        if(i%2 == 1){
          data_values.push(parseFloat(value))
        }
      })

      data_object = {}
      $(data_values).each(function(i,e){
        data_object[data_values[i]] = data_descriptions[i]
      })
      data_values = data_values.sort(sortNumber);    

      sorted = []
      sorted.push(data_object[data_values[1]])
      sorted.push(data_object[data_values[2]])
      $(table_el).siblings('p').find('.text1').text(sorted[0])
      $(table_el).siblings('p').find('.text2').text(sorted[1])
    }

  return this;
}.call({});
