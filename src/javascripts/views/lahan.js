// obelus-c3-chart
window.twentyfifty.views.lahan = function() {
  
  this.setup    = function(){
    $('#ob-chart-container, #classic_controls').show()
    $('#ob-mini-paper, #energysecurity, #cost-macro-micro').hide()
  }
  
  this.teardown = function(){}

  this.updateResults = function(pathway){
    by_sector     = pathway.land.by_sector;
    total_land    = pathway.land.total_land;
    food_security = pathway.land.food_security;
    
    $('.ob-chart').show();
    
    max1 = obelusMaxValueOfArrayNumber(by_sector)
    max2 = obelusMaxValueOfArrayNumber(total_land)
    max_val = Math.max(max1, max2)

    if(typeof(activechart) != 'undefined' && activechart.length > 0 && window.location.pathname.split('/')[3] == view){
      chart = new obelusChart();
      chart.updateStacked('land.by_sector','Luas Lahan Berdasarkan Sektor', pathway.land.by_sector, 'area', max_val)
      chart.updateStacked('land.total_land','Luas Lahan Berdasarkan Total',  pathway.land.total_land, 'area', max_val)
      chart.updateStacked('land.food_security','Ketahanan Pangan Kalori',    pathway.land.food_security, 'area', null)
    }else{
      chart = new obelusChart();
      chart.stacked('land.by_sector','Luas Lahan Berdasarkan Sektor', pathway.land.by_sector, '#ob-chart1', 'area', 'Juta hektar', max_val)
      chart.stacked('land.total_land','Luas Lahan Berdasarkan Total',  pathway.land.total_land, '#ob-chart2', 'area', 'Juta hektar', max_val)
      chart.combination('land.food_security','Ketahanan Pangan Kalori',    pathway.land.food_security, '#ob-chart3', 'area', 'Milyar kalori')
    }
  }

  return this;
}.call({});
