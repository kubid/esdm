$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

$('#fullpage').fullpage({
    anchors:['home','perubahan-iklim', '2-derajat', 'kebijakan-indonesia', 'calculator-2050', 'about'],
    navigation: true,
    sectionsColor : ['#FFF', '#FFF', '#5DC1C1', '#FFF', '#5DC1C1', '#FFF'],
    slidesNavigation: true,
    navigationTooltips: ['Home','Perubahan Iklim', '2 Derajat', 'Kebijakan Indonesia', 'Calculator 2050'],
    onLeave: function(index, nextIndex, direction){
        //leaving 1st section
        if(nextIndex == 1){
            $('.subcontent-list').removeClass('fixed');
        }
        //back to the 1st section
        if(index == 1){
           $('.subcontent-list').addClass('fixed');
        }
    }   
})

(function(){
  function id(v){ return document.getElementById(v); }
  function loadbar() {
    var ovrl = id("overlay"),
        prog = id("progress"),
        stat = id("progstat"),
        img = document.images,
        c = 0,
        tot = img.length;
    if(tot == 0) return doneLoading();

    function imgLoaded(){
      c += 1;
      var perc = ((100/tot*c) << 0) +"%";
      prog.style.width = perc;
      stat.innerHTML = "Loading "+ perc;
      if(c===tot) return doneLoading();
    }
    function doneLoading(){
      ovrl.style.opacity = 0;
      setTimeout(function(){ 
        ovrl.style.display = "none";
      }, 1200);
    }
    for(var i=0; i<tot; i++) {
      var tImg     = new Image();
      tImg.onload  = imgLoaded;
      tImg.onerror = imgLoaded;
      tImg.src     = img[i].src;
    }    
  }
  document.addEventListener('DOMContentLoaded', loadbar, false);
}());