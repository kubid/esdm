function initToggle(){
	titles = $('#classic_controls h2, #classic_controls h3');
	$.each(titles, function(i,e){
		nextEl = $(this).next()

		// if(nextEl.is('table')){
			$(this).addClass("clickable-toggle")
			$(this).append('<i class="arrow"></i>')
			$(this).click(function(){
				nextEl = $(this).next()
				$(nextEl).toggle()
				$(this).toggleClass('hidden-down')
			})
		// }
	})	
}

function initCatatan(){
	// $('#notes_container').draggable({ revert: true });
  window.note_dispayed = 0;
	$('#notes_container h4').click(function(){
		$('#notes_container').animate({right: -400})
	})
	$('#display_catatan').click(function(){
    if(note_dispayed == 0){
      $('#notes_container').animate({right: 0})
      window.note_dispayed = 1
    }else{
      $('#notes_container').animate({right: -400})
      window.note_dispayed = 0
    }
		
	})
	
	
}


function toggleLand(){
	$('#food_security2').hide();
	$('#food_security .charttitle').click(function(){
		$('#food_security').hide();
		$('#food_security2').show();
	})
	$('#food_security2 .charttitle').click(function(){
		$('#food_security2').hide();
		$('#food_security').show();
	})
}

function sankeyNav(){
	$('#sankey-nav li a').on('click touchend', function(event) {
		var t, v;
    event.preventDefault();
		$('#sankey-nav li a').removeClass('active')
		$(this).addClass('active')
    t = $(event.target);
    v = t.data().view;
    return switchView(v);
	})
}

function submitPathway() {
  togglePopupWindow('#submit-pathway', '#ob-submit-pathway')
	
	$('#ob-submit-pathway form').ajaxForm({
		url: '/submit-pathway', 
		type: 'post',
		beforeSubmit: validate,
		success: function(data){		
    $('#ob-submit-pathway').fadeOut('fast')	
      if(data.success){
        $('#ob-submit-pathway form [type!="submit"]').val("")
        $('#ob-submit-pathway').fadeOut('fast')
      }
      $('#ob-submit-pathway form [type="submit"]').val("Submit")
      grecaptcha.reset();
			alert(data.message)
		}
	})

	function validate(formData, jqForm, options) { 
    $('#ob-submit-pathway form [type="submit"]').val("Memproses..")
    // formData is an array of objects representing the name and value of each field 
    // that will be sent to the server;  it takes the following form: 
    // 
    // [ 
    //     { name:  username, value: valueOfUsernameInput }, 
    //     { name:  password, value: valueOfPasswordInput } 
    // ] 
    // 
    // To validate, we can examine the contents of this array to see if the 
    // username and password fields have values.  If either value evaluates 
    // to false then we return false from this method. 
    captcha = false
    for (var i=0; i < formData.length; i++) { 
        if (!formData[i].value) { 
        	arr = []
          
					$.each(formData, function(i,e){
            if(e.name == "g-recaptcha-response"){
              captcha = formData[i].value.length > 0
            }else{
              name = $('[name="'+e.name+'"]').data('id')
              if(e.value == "" && name != "undefined"){
                arr.push(name) 
              }
            }
					})
          message = ''
          if(arr.length > 0){
            message = 'Field '+ arr.join(", ")+' harus diisi'
            if(!captcha){message = message+ '\n\n'}
          }
          if(!captcha){
            message = message + 'Pastikan verifikasi "I\'m not a robot diisi" dengan benar'
          }
          alert(message); 
          $('#ob-submit-pathway form [type="submit"]').val("Submit")
          return false; 
        } 
    }
    
  }
}

function sharePathway() {
  togglePopupWindow('#share-pathway', '#ob-share-pathway')
}

function togglePopupWindow(class_el, target_el){
  $(class_el).click(function(){
    $(target_el).fadeIn('fast')
    if($(target_el).is(':visible')){
      $(target_el+', '+target_el+ ' .close').click(function(){
        $(target_el).fadeOut('fast')
      })  
      $(target_el+ ' #msform').click(function(event){
        event.stopPropagation();
      })
    }
  })
}

function fbsharePathwayUrl(){
  share_url = window.location.origin + '/pathways/'+ choices.join("")
  window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(share_url)+"&t="+document.title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
  return false; 
}

function twittersharePathwayUrl() {
  share_url = window.location.origin + '/pathways/'+ choices.join("")
  username ='kementerianesdm'
  window.open('https://twitter.com/share?url='+escape(share_url)+'&text='+document.title +', '+ share_url+ ' via @' + username, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');

  return false
}

function buttonFacebookShare(){
  $('.btn-fb').click(function(){
    FB.login(function(response) {
    if (response.authResponse) {
      access_token = FB.getAuthResponse()['accessToken'];
      body = $('.bubble textarea').val() || "Hi, Saya baru saja menggunakan kalkulator pathway 2015 indonesia\nhttp://calculator2050.esdm.go.id/"    
      FB.api('/me/feed', 'post', { message: body, access_token: access_token }, function(response) {
        $('#ob-share-pathway').fadeOut('fast')
        if (!response || response.error) {
          alert('Pathway gagal di bagikan');
        } else {
          alert('Pathway berhasil di bagikan');
        }

      });
      } else {
       console.log('User cancelled login or did not fully authorize.');
      } 
  }, {scope: 'publish_actions'});    
  })

}
function initWelcomeMessage(){
  target_el = '#ob-welcome-message'
  $(target_el).fadeIn('fast')
  $(target_el+' .close').click(function(){
    $(target_el).fadeOut('fast')
  })  
}


function initTour(){
  localStorage.clear();
  $('#start-tour').click(function(){
    var tour = new Tour({
      backdrop: true,
      smartPlacement: false,
      name: "app-tour",
      steps: [
      {
        element: "#main-menu",
        title: "Menu tampilan",
        placement: 'bottom'
      },
      {
        element: "#classic_controls",
        title: "Pemilihan Skenario",
        placement: 'top'
      },
      {
        element: "#float-menu",
        title: "Contoh Skenario",
        placement: 'left'
      },
      {
        element: "#r21",
        title: "Contoh Skenario",
        placement: 'bottom'
      }
    ]});

    tour.init();
    tour.start()
  })
}