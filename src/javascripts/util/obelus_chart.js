window.obelusChart = function() {
  window.activechart = [];

  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  };

  function checkINDC(myArray, num) {
    myLength = myArray.length
    if (myArray.slice(myLength - 1, myLength) == "Baseline" || myArray.slice(myLength - 1, myLength) == "Rasio pasokan bioenergi") {
      return num = -1;
    } else {
      return num = myLength;
    }
  };

  function checkString(myArray, myString, num) {
    var searchedString = myArray.check(myString)
    return num = myArray.find(myString)
  };

  this.combination = function(chart_name, chart_title, data_x, elem_id) {
    var dup_data = data_x.slice()
    var categories = dup_data[0].slice()
    var categories = categories.splice(1)

    var data_columns = this.filterData(dup_data);
    activechart.push(chart_name)
    var ochart = window[chart_name.replace(/./g, '_')] = c3.generate({
      bindto: elem_id,
      data: {
        columns: data_columns,
        type: 'bar',
        types: {
          "Tingkat ketersediaan": 'spline',
          "Rasio pasokan bioenergi": 'line',
        },
        colors: this.chartColors(),
        axes: {
          "Tingkat ketersediaan": 'y2',

        }
      },
      axis: {
        x: {
          type: 'category',
          categories: categories
        },
        y: {
          label: 'Milyar Kalori',
          padding: {
            bottom: 0
          },
          tick: {
            format: function(y) {
              if (y <= 10.0 && y !== 0.0 && y >= -10.0) {
                return d3.format('.1f')(y);
              } else {
                return d3.format(',')(y);
              }
            }
          },
        },
        y2: {
          label: "Persen",
          min: 0,
          padding: {
            bottom: 0,
            top: 0
          },
          tick: {
            format: function(y2) {
              if (y2 <= 1.0 && y2 !== 0.0) {
                return d3.format('.0f')(y2 * 100);
              }
            }
          },
          show: true
        }
      },
      zoom: {
        enabled: true
      },
      tooltip: {
        format: {
          title: function(d) {
            return 'Tahun ' + categories[d];
          },
          value: function(value, ratio, id) {
              var format = id === 'Tingkat ketersediaan' ? d3.format('.1%')(value) : d3.round(value, 2);
              return format
            }
            //            value: d3.format(',') // apply this format to both y and y2
        }
      },
      color: {
        // pattern: ['#9F3100', '#E56D38', '#F1CD78', '#DEDB95']
      },
      grid: {
        x: {
          show: false
        },
        y: {
          show: true
        }
      },
      legend: {
        show: false
      }
    });
    d3.select(elem_id).insert("div", ":first-child").attr('class', 'ob-chart-title')
    $(elem_id + ' .ob-chart-title').text(chart_title)
    groups = []
    $.each(dup_data, function(i, e) {
      groups.push(e[0])
    })
    this.customLegend(elem_id, groups, ochart)
    this.appendResetZoom(elem_id, ochart)
  }

  this.combination_2 = function(chart_name, chart_title, data_x, elem_id) {
    var dup_data = data_x.slice()
    var categories = dup_data[0].slice()
    var categories = categories.splice(1)


    var data_columns = this.filterData(dup_data);
    console.log('--------MyComboData:' + data_columns)
    activechart.push(chart_name)
    var ochart = window[chart_name.replace(/./g, '_')] = c3.generate({
      bindto: elem_id,
      data: {
        columns: data_columns,
        type: 'bar',
        colors: this.chartColors(),
        groups: [
          ["Biaya OPEX", "Biaya CAPEX"],
          ["Sektor Permintaan", "Sektor Ketenagalistrikan"]
        ],
        types: {
          "Kumulatif CAPEX": 'line',
          "Total": 'line',
        },
        axes: {
          "Kumulatif CAPEX": 'y',
          "Total": 'y2',
        }
      },
      point: {
        r: 3,
      },
      axis: {
        x: {
          type: 'category',
          categories: categories,
          tick: {
            rotate: 90,
            multiline: false
          }
        },
        y: {
          label: "Trilliun Rp",
          max: 12000,
          min: 0,
          padding: {
            bottom: 0
          },
          tick: {
            format: d3.format(',')
          }
        },
        y2: {
          show: false,
          label: "Trilliun Rp",
          max: 12000,
          min: 0,
          padding: {
            bottom: 0
          },
          tick: {
            format: function(y2) {
              if (y2 <= 0.99 && y2 !== 0.0 && y2 >= -0.99) {
                return d3.format('.2f')(y2);
              } else {
                return d3.format(',')(y2);
              }
            }
          },
        }
      },
      zoom: {
        enabled: true
      },
      tooltip: {
        format: {
          title: function(d) {
            return 'Tahun ' + categories[d];
          },
          value: function(value, ratio, id) {
              var format = id === 'Kumulatif' ? formatNumber(d3.round(value, 2)) : formatNumber(d3.round(value, 2));
              return format
            }
            //            value: d3.format(',') // apply this format to both y and y2
        }
      },
      color: {
        //

      },
      grid: {
        x: {
          show: false
        },
        y: {
          show: true
        }
      },
      legend: {
        show: false
      }
    });
    d3.select(elem_id).insert("div", ":first-child").attr('class', 'ob-chart-title')
    $(elem_id + ' .ob-chart-title').text(chart_title)
    groups = []
    $.each(dup_data, function(i, e) {
      groups.push(e[0])
    })
    this.customLegend(elem_id, groups, ochart)
  }




  this.stacked = function(chart_name, chart_title, chart_data, elem_id, chart_type, label_y, max_axis) {
    console.log("chart_data-------------------------------")
    setup = this.setup(chart_data, chart_type)
    setup.data_columns.splice(setup.data_columns.length, 1)
    var lineINDC = checkINDC(setup.data_groups, lineINDC)
    var thirdLine = setup.data_groups.slice(-1, setup.data_groups.length)
    var label_y2 = '%'

    console.log('--------Label------:' + label_y == '%' || label_y2 == '%')

    y_label_opt = {
      label: label_y,
      position: 'outer-middle'
    }


    if (max_axis !== undefined) {
      //y_label_opt.max = max_axis 
      label_y.max = max_axis
    }

    setup.data_groups.pop()


    var formatter;

    function updateFormatter(myvalue) {
      formatter = d3.format(myvalue <= 1.0 ? ',.2f' : ',')
    }

    function getInfo(num_1, num_2) {
      if (num_1 == "Rasio pasokan bioenergi") {
        return num_2 = true;
      } else {
        return num_2 = false;
      }
    }

    var showAxis = getInfo(thirdLine, showAxis)


    activechart.push(chart_name)
    var ochart = window[chart_name.replace(/\./g, '_')] = c3.generate({
      bindto: elem_id,
      data: {
        columns: setup.data_columns,
        type: chart_type,
        types: {
          'Total': 'line',
          'Total lahan': 'line',
          'Baseline': 'line',
          'Total pembangkitan': 'line',
          'Rasio pasokan bioenergi': 'line',
          'Kumulatif CAPEX': 'line',
        },
        axes: {
          'Rasio pasokan bioenergi': 'y2',
          'Total lahan': 'y',
        },
        groups: [setup.data_groups.slice(0, lineINDC)],
        //order: 'asc',
        colors: this.chartColors()
      },
      point: {
        r: 5
      },

      axis: {
        x: {
          type: 'category',
          categories: setup.categories,
          tick: {
            rotate: 90,
            multiline: false
          },
        },
        y: {
          max: max_axis,
          padding: {
            bottom: 0
          },
          label: label_y,
          tick: {
            format: function(y) {
              if (y <= 0.99 && y !== 0.0 && y >= -0.99) {
                return d3.format('.2f')(y);
              } else {
                return d3.format(',')(y);
              }
            }
          }

        },
        y2: {
          //max: 0.1,
          label: label_y2,
          min: 0,
          tick: {
            format: function(y2) {
              if (y2 <= 1.0 && y2 !== 0.0) {
                return d3.format('.1f')(y2 * 100);
              }
            }
          },
          padding: {
            bottom: 0,
            top: 0
          },
          show: showAxis

        }
      },
      color: {
        // pattern: ['#FF001E', '#FF6200', '#FF8000', '#FFB300', '#FFD000']
      },
      tooltip: {
        grouped: true, // default true
        format: {
          title: function(d) {
            return 'Tahun ' + categories[d];
          },
          value: function(value, ratio, id) {
              if (id === 'Rasio pasokan bioenergi') {
                return d3.format('.2%')(value)
              } else {
                return formatNumber(value.toFixed(2)) + " " + label_y
              }
            }
            //            value: d3.format(',') // apply this format to both y and y2
        }
      },
      grid: {
        x: {
          show: false
        },
        y: {
          show: true
        }
      },
      zoom: {
        enabled: true
      },
      point: {
        show: chart_title == "Ketergantungan Impor" ? true : false
      },
      legend: {
        show: false
      }


    });
    d3.select(elem_id).insert("div", ":first-child").attr('class', 'ob-chart-title')
    $(elem_id + ' .ob-chart-title').text(chart_title)
    this.customLegend(elem_id, setup.groups, ochart)
    this.appendResetZoom(elem_id, ochart)
  }

  this.updateStacked = function(chart_name, chart_title, chart_data, chart_type, max_axis) {
    setup = this.setup(chart_data, chart_type)
    setup.data_columns.splice(setup.data_columns.length, 1)
    dchart = window[chart_name.replace(/\./g, '_')]
    dchart.load({
      columns: setup.data_columns
    })
    if (max_axis != null) {
      dchart.axis.max(max_axis)
    }
    activechart.push(chart_name)
  }

  this.appendResetZoom = function(elem_id, chart) {
    d3.select(elem_id).insert('div').attr('class', 'reset-zoom').selectAll('span')
      .data([elem_id])
      .enter().append('div')
      .attr('data-id', function(id) {
        return id;
      })
      .html(function(id) {
        return '<a href="javascript:void(0)" class="btn-reset-zoom zoom-chart-' + id + '">Reset</a>'
      })
      .on('click', function(id) {
        chart.internal.updateAndRedraw()
      });
  }

  //  custom legend
  this.customLegend = function(elem_id, groups, chart) {
    d3.select(elem_id).insert('div', '.chart').attr('class', 'ob-chart-legend').selectAll('span')
      .data(groups)
      .enter().append('div')
      .attr('data-id', function(id) {
        return id;
      })
      .html(function(id) {
        return '<span class="block" style="background-color:' + chart.color(id) + '"></span><span class="text-legend">' + id + '</span>'
      })
      .each(function(id) {
        // new_text = this.textContent.replace("Pembangkit listrik ", "")
        // text = this.textContent
        d3.select(this).attr('class', 'ob-legend-text-container')
        d3.select(this).append('div').attr('class', 'clearfix')
      })
      .on('mouseover', function(id) {
        chart.focus(id);
      })
      .on('mouseout', function(id) {
        chart.revert();
      })
      .on('click', function(id) {
        $('[data-id="' + id + '"]').toggleClass('disabled-legend')
        chart.toggle(id);
      });
    lHeight = $(elem_id).children('.ob-chart-legend').height() + 75
    $(elem_id).css('margin-bottom', lHeight + 'px')
  }

  //  setup parameters
  this.setup = function(data_ori, chart_type) {
    dup_data = data_ori.slice()
    last_elem = dup_data[dup_data.length - 1][0]
      // if (last_elem.toLocaleLowerCase().indexOf("total") == 0) {
      //     dup_data.splice(dup_data.length - 1, 1)
      // }

    cat = dup_data.splice(0, 1)[0]
    types = {}, groups = [], data_groups = []
    categories = cat.slice()
    categories.splice(0, 1)
      //console.log('--------MyCatData:'+cat)
      //console.log('--------MyDupData:'+dup_data)

    $.each(dup_data, function(i, e) {
      types[e[0]] = chart_type
    })
    $.each(dup_data, function(i, e) {
      groups.push(e[0])
      if (chart_type != 'line') {
        data_groups.push(e[0])
      }
    })

    return {
      data_columns: dup_data,
      categories: categories,
      chart_type: types,
      groups: groups,
      data_groups: data_groups
    }
  }

  this.chartColors = function() {
      colors = {
        'Batubara': '#604343',
        'Produksi batubara': '#000000',
        'Impor batubara': '#7f7f7f',
        'Minyak bumi': '#111E06',
        'Gas bumi': '#3e96cc',
        'Gas turbin': '#b9d9fb',
        'Panas bumi': '#ff7b22',
        'Rumah tangga': '#ffb255',
        'Rumah Tangga': '#ffb255',
        'Komersial': '#ff542c',
        'Transportasi': '#a0a0a0',
        'Industri': '#9cb7d4',
        'Pertanian, kontruksi dan pertambangan': '#604343',
        'Luas lahan lainnya': '#ddc8c7',
        'Perkebunan non-kelapa sawit': '#BCBD22',
        'Luas hutan tersisa': '#7ec474',
        'Pertanian & perkebunan': '#668a62',
        'Pertanian dan perkebunan': '#cdf92d',
        'Perkebunan kelapa sawit': '#f97338',
        'Lahan kritis': '#ff3939',
        'Lahan bioenergi': ' #54efca',
        'Bioenergi non-sawit': '#abd8cc',
        "Penggunaan lahan lainnya": '#1F77B4',
        "Perkebunan kelapa sawit": '#FC7458',
        "Bioenergi - sawit": '#54efca',
        "Pertanian": '#E377C2',
        "Lahan HTI": '#b7e57c',
        "Luas lahan HTI": '#818c3c',
        "Lahan HPH": '#ceb815',
        "Luas lahan HPH": '#a5815e',
        "Biaya CAPEX": '#FCAB58',
        "Biaya OPEX": '#D62728',
        "Kumulatif CAPEX": '#0F66C7',
        "Tambahan pembangkit": '#82f9c0',
        "Tambahan pembangkit listrik": '#4d4df4',
        "Pertanian, konstruksi dan pertambangan": '#604343',
        "Baseline": 'red',
        "Total pembangkitan": '#123456',
        "Total": '#123456',
        "Rasio pasokan bioenergi": 'blue',
        "Pembangkitan berbasis energi fosil": '#bc9c6c',
        "Permintaan kalori": '#ff542c',
        "Pasokan kalori": '#fcf060',
        "Tingkat ketersediaan": '#00dbd0',
        "Produksi H2": '#0055a5',
        "Distribusi dan transmisi listrik": '#6666ff',
        "Hidro": '#0055a5',
        "Biomasa": '#a5e57c',
        "EBT lainnya": '#ace5cb',
        "Gas combined-cycle": '#c6c6c6',
        "Diesel": '#49db49',
        "Nuklir": '#fff001',
        "Sektor Permintaan": '#ff542c',
        "Sektor Ketenagalistrikan": '#4d4df4',
        "Lahan pertanian dan perkebunan": '#cdf92d',
        "Lahan kehutanan": '#10a300',
        "Lahan perkebunan kelapa sawit": '#f97338',
        "Pemanfaatan energi": '#fff001',
        "Tata guna lahan": '#19f47b',
        "Biaya CAPEX": "#93bc7e",
        "Biaya OPEX": "#6ea5f4",
        "Kumulatif CAPEX": "#00dbd0"
      }
      return colors
    }
    //  filter_data
  this.filterData = function(data) {
    data.splice(0, 1);

    $.each(data, function(i, e) {
      if (i == 2) {
        data.push([])
        $.each(data[i], function(idx, x) {
          if (idx > 0) {
            x = x * 1
          }
          data[i + 1].push(x);
        })
      }
    })
    data.splice(2, 1);
    return data
  }
}
